﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace HWEventsDelegates
{
    public class FileSearch 
    {
        private string patch;
        public FileSearch(string patch)
        {
            this.patch = patch;
        }

        public void SearchFiles()
        {
            DirectoryInfo directory = new DirectoryInfo(patch);
            foreach (var file in directory.GetFiles())
            {
                var fileArgs = new FileArgs(file.Name);
                EventFoundFiles?.Invoke(this, fileArgs);
                if (fileArgs.CancelRequested)
                    break;
            }
        }

        public event EventHandler<FileArgs> EventFoundFiles;
    }
}

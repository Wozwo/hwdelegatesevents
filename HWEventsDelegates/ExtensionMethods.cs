﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HWEventsDelegates
{
    public static class ExtensionMethods
    {
        public static T WTFMax<T>(this IEnumerable<T> list, Func<T, float> func)
        {
            var max = float.MinValue;
            T t = default;
            foreach (var item in list)
            {
                var x = func(item);
                if (x > max)
                {
                    max = x;
                    t = item;
                }
            }
            return t;
        }
    }
}

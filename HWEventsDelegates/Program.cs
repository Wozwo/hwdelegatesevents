﻿using System;
using System.IO;

namespace HWEventsDelegates
{
    class Program
    {
        static void Main(string[] args)
        {
            //Поиск максимального элемента
            TestC[] testC = new TestC[] { new TestC(1), new TestC(500), new TestC(323), new TestC(611), new TestC(8), new TestC(451) };
            var maxFloatObject = testC.WTFMax(x => x.I);
            Console.WriteLine(maxFloatObject);

            //Поиск файлов с функционалом отмены.
            FileSearch fileSearch = new FileSearch(Environment.CurrentDirectory);
            fileSearch.EventFoundFiles += FileSearch_EventFoundFiles;
            fileSearch.SearchFiles();

            Console.ReadKey();
        }

        private static void FileSearch_EventFoundFiles(object sender, FileArgs file)
        {
            Console.WriteLine($"\nНайден файл: {file.FileName}");
            if (file.FileName.Contains(@".exe"))
                file.CancelRequested = true;
        }
    }
}

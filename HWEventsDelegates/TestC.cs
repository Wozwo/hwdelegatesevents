﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HWEventsDelegates
{
    public class TestC
    {
        public float I { get; private set; }

        public TestC(float i)
        {
            I = i;
        }

        public override string ToString()
        {
            return $"Экземпляр класса TestC с значением свойства I = {I}";
        }
    }
}

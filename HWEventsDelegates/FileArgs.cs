﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HWEventsDelegates
{
    public class FileArgs : EventArgs
    {
        public string FileName { get; }
        public bool CancelRequested { get; set; }
        public FileArgs(string fileName)
        {
            FileName = fileName;
        }
    }
}
